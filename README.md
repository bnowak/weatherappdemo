
# About 
This is a response to the [following task](https://github.com/buildit/org-design/blob/master/Recruitment/Exercises/js_engineer.md)

# Implmentation plan
    [ ] - Create implmentation plan (00:15:00)
    [x] - Create UX wireframes (out of scope === OOS)
    [x] - Create Visual Designs (OOS)
    [x] - Agree the API contract (OOS)
    [x] - Generate DTOs/models using tools like protobuf or transit.js
    [x] - Prepare basic project structure (00:45:00)
        Acceptance criteria:
            [x] - as a developer I want to use full ES6 features with Babel and a module loader like Webpack or System.js
            [x] - as a developer I want to use React as my View framework
            [x] - as a developer I want to have HMR available for both js and CSS modules (as opposed to LiveReload and similar ilk)
            [x] - as a developer I would like to use any Jasmine/Karma/Tape/Enzyme/Sinon/Chai/Mocha combination for unit tests
            [ ] - as a developer I would like to see my coverage while running tests - out of time, not implmented
            [ ] - as a developer I would like to use SASS or Stylus for css  - out of time, not implmentedprocessing
    [x] - Implement basic UI components structure (01:00:00)
    [ ] - Implement unit tests - out of time, not implmented
    [ ] - Manual E2E testing
    [ ] - Implement CSS regression tests (OOS)
    [ ] - Implement E2E tests (OOS)
    [ ] - Implement accessiblity tests (pa11y)(OOS)
    [ ] - Implement target platform testing (browserlabs, etc)(OOS)
    [ ] - Implement performance tests (Sitespeed.io, etc)(OOS)
    [ ] - Implment DevOps integration (jenkins/circleCI, etc)
    [ ] - Implment static analysis against technical debt (SonarQube, etc)(OOS)

# Usage

**Step 1.** Make sure that you have Node.js v6 or newer installed on your machine.

**Step 2.** Clone this repo

**Step 3.** Compile and launch your app by running:
```
$ node run                      # Same as `npm start` or `node run start`
```

You can also test your app in release (production) mode by running node run start --release or with HMR and React Hot Loader disabled by running node run start --no-hmr. The app should become available at http://localhost:3000/.

# Testing

The unit tests are powered by chai and mocha.
```
$ npm run lint                  # Check JavaScript and CSS code for potential issues
$ npm run test                  # Run unit tests. Or, `npm run test:watch`
```

# Implmentation notes

Total implmentation time: 02:00:00;

## Project structure
Since this is only a demonstration we'll try to use one of the 'accelerators' available on GitHub. On a production project, we'd want to start from scratch to fine tune for the project needs.
In this particular case we don't want to use isomorphic app, as we aim to serve the dist artifact from a static server.
[This accelerator](https://github.com/kriasoft/react-static-boilerplate) seems to fit the bill. 
Directory structure is inherited from the accelerator.

## Features
The idea was to display google maps with a marker assigned to user's ge-located position and allowing to further search. I've managed to implment basic API integration.
However - ther's some issue with gMaps display.

## How can I make it better
See out-of-scope notes in the implmentation plan.
Additionally stuff like Immutable.js, etc