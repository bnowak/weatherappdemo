
import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Layout from '../../components/Layout';
import s from './styles.css';
import GoogleMapsLoader from 'google-maps';
import $ from 'jquery';



/// So secure
let key = "98c355d73f22c6eb33c4bc0bd22031fe";
GoogleMapsLoader.KEY = "AIzaSyCVyvJuOwkWY8k4heY9IF3mE2LdK_8UOqk";

/// Google Map Vars
let google = null;
let map = null;
let marker = null;
let mapZoomLevel = null;

export default class WeatherApp extends React.Component {

  static propTypes = {
    initialLat: React.PropTypes.number,
    initialLon: React.PropTypes.number,
    mapZoomLevel: React.PropTypes.number
  };

  static initialProps = {
    initialLat: 51.75,
    initialLon: -3.38,
    mapZoomLevel: 10
  };

  state = {
    /// Passed as props on render
    'lat': WeatherApp.initialProps.initialLat,
    'lon': WeatherApp.initialProps.initialLon,
    /// These will be updated with data from the API, so are subject to change (point 2)
    'location': '', 
    'weather':  '',
    'icon':     ''
  };

  /**
   * After initial render
   */
  componentDidMount = () => {
    /// Render a new map
    this.renderMap();
    /// Run update state, passing in the setup
    this.updateState(null, this.state.lat, this.state.lon);
  }

  /**
   * Request data from the API
   * No fail checks! >.<
   */
  getData = (location, lat, lon) => {
    
    /// Variable to return
    var data;
    
    /// If it's a search
    if (location !== null)
    {
      data = $.get('http://api.openweathermap.org/data/2.5/weather?q=' + location + '&APPID=' + key)
    }
    else /// It's a pin drop
    {
      data = $.get('http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&APPID=' + key)
    }    
    return data;
  }

  /**
   * Capitalize the first letter of a string
   */
  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  /**
   * Update state
   */
  updateState = (locationName, lat, lon) => {
    /// Get data from the API, then set state with it
    this.getData(locationName, lat, lon)
      .then(function(data) {
        if(data.cod === 404) throw new Error( data.message );
        /// Update the state, pass updateMap as a callback
        this.setState({
          lat:      data.coord.lat,
          lon:      data.coord.lon,
          weather:  this.capitalizeFirstLetter( data.weather[0].description ),
          location: data.name,
          icon:     'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png' /// Messy
        }, this.updateMap ) /// Pass updateMap as a callback
      }.bind(this));
  }

  /**
   * Run when 'search' button is pressed
   */
  locationSearch = () => {
    /// Get the value from the search field
    var location = ReactDOM.findDOMNode(this.refs.newLocation).value;
    
    if ( location !== '' )
    {
      /// Update state with new API Data based on location name
      this.updateState(location, null, null);
    }
  }

  /**
   * Run when 'location' button is used
   */
  geolocationSearch = () => {
    
    /// Successful geolocation
    var success = function (position) {
      var lat = position.coords.latitude;
      var lon = position.coords.longitude;
      
      /// Update state with new API Data based on lat lon
      this.updateState(null, lat, lon);
    }.bind(this);
  
    /// Error'd geolocation
    var error = function (error) {
      if (error.message == 'User denied Geolocation')
      {
        alert('Please enable location services');
      }
    };
    
    /// Get the position
    navigator.geolocation.getCurrentPosition(success, error);
  }

  /**
   * Run when the form is submitted
   */
  formSubmit = (e) => {
    e.preventDefault();    
    /// Clear the input
    ReactDOM.findDOMNode(this.refs.newLocation).value = '';
  }

  /**
   * Render the map on the page
   */
  renderMap = (lat, lng) => {

    GoogleMapsLoader.load( g => {
      google = g;

      /**
       * Map coordinates and pin coordinates are added in updateMap(),
       * which is run by updateStateWithData()
       */
      
      /// Create a new map
      map = new g.maps.Map(document.getElementById('map'), {
        zoom: this.props.mapZoomLevel,
        disableDefaultUI: true,
        zoomControl: true
      });
    
      /// Create a new marker
      marker = new g.maps.Marker({
        map: map,
        draggable: true
      });
      
      /// Set the initial pin drop animation
      marker.setAnimation(g.maps.Animation.DROP);
    
      /// Add an event listener for click
      g.maps.event.addListener(map, 'click', function(event) {
        var latLng = event.latLng;
        var lat = latLng.lat();
        var lng = latLng.lng();
        
        /// Update state based on lat lon
        this.updateState(null, lat, lng)
      }.bind(this));
      
      /// Add an event listener for drag end
      g.maps.event.addListener(marker, 'dragend', function(event) {
        
        var latLng = event.latLng;
        var lat = latLng.lat();
        var lng = latLng.lng();
        /// Update state based on lat lon
        this.updateState(null, lat, lng)
      }.bind(this));
      
      /// Update variable on map change
      map.addListener('zoom_changed', function() {
        mapZoomLevel = map.getZoom();
      });
    });
  }

  /**
   * Set map marker position and pan settings
   */
  updateMap = (lat, lon) => {

    if(!google)
      return null;

    var latLng = new google.maps.LatLng(this.state.lat, this.state.lon);
    
    /// Set a timeout before doing map stuff
    window.setTimeout( function() {
      
      /// Set the marker position
      marker.setPosition(latLng);
      
      /// Pan map to that position
      map.panTo(latLng);
    }.bind(this), 300);
  }

  /**
   * Render the app
   */
  render = () => {
    return (
      <div id="app">
        <div id="app__interface">
          <div className="panel panel-default">
            <div className="panel-heading text-center"><span className="text-muted">Enter a place name below</span></div>
              <div className="panel-body">
                { /* Search Form - Ideally this should be moved out */ }
                <form onSubmit={this.formSubmit}>
                    <div className="input-group pull-left">
                      <input type="text" className="form-control" placeholder="Enter a town/city name" ref="newLocation"/>
                      <span className="input-group-btn">
                        <button type="submit" className="btn btn-default" onClick={this.locationSearch}>Search</button>
                      </span>
                    </div>
                    
                </form>
              </div>
            <WeatherDetails location={this.state.location} weather={this.state.weather} icon={this.state.icon} />
          </div>
        </div>
        <div id="map"></div>
      </div>
    );
  }

};

export class WeatherDetails extends React.Component {
  render() {
    return (
      <div className="panel-heading weather">
        <p className="text-muted"><strong>{this.props.location}</strong></p>
        <span className="text-muted">{this.props.weather}</span>
        <div className="weather__icon">
          <img src={this.props.icon} />
        </div>
      </div>
    )
  }
}
